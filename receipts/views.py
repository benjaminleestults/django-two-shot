from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from receipts.models import Receipt, Account, ExpenseCategory
from receipts.forms import ReceiptForm, CategoryForm, AccountForm


# Create your views here.
# A List of Receipts(instances)
@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": receipts,
    }
    return render(request, "receipts/list.html", context)


# A Create Recipe view (login required)
@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


# A list of Account(s) (login required)
@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts,
    }
    return render(request, "accounts/list.html", context)


# A list of Category(ies) (login required)
@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": categories,
    }
    return render(request, "categories/list.html", context)


# Create a Category (login required)
@login_required
def create_category(request):
    form = CategoryForm(request.POST)
    if form.is_valid():
        category = form.save(False)
        category.owner = request.user
        category.save()
        return redirect("category_list")
    else:
        form = CategoryForm()
    context = {
        "form": form,
    }
    return render(request, "categories/create.html", context)


# Create an Account (login required)
@login_required
def create_account(request):
    form = AccountForm(request.POST)
    if form.is_valid():
        account = form.save(False)
        account.owner = request.user
        account.save()
        return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/create.html", context)
